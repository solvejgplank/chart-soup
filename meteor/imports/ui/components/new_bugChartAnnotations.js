import React, { Component } from 'react';
import { TwitterPicker as ColorPicker } from 'react-color';
import { updateAndSave, dataParse, isNumber } from '../../modules/utils';
import { app_settings } from '../../modules/settings';
import { parse } from '../../modules/chart-tool';
import Swal from 'sweetalert2';
import Slider from 'rc-slider';

export default class ChartAnnotations extends Component {

  constructor(props) {
    super(props);
    this.resetDefaultAnnoSettings = this.resetDefaultAnnoSettings.bind(this);
    this.toggleSubsectionExpand = this.toggleSubsectionExpand.bind(this);
    this.resetAnnotations = this.resetAnnotations.bind(this);
    this.removeHighlight = this.removeHighlight.bind(this);
    this.editRange = this.editRange.bind(this);
    this.removeRange = this.removeRange.bind(this);
    this.setRangeConfig = this.setRangeConfig.bind(this);
    this.formatRangeValue = this.formatRangeValue.bind(this);
    this.renderRangeValueInput = this.renderRangeValueInput.bind(this);
    this.setRangeValue = this.setRangeValue.bind(this);
    this.setTextConfig = this.setTextConfig.bind(this);
    this.editText = this.editText.bind(this);
    this.removeText = this.removeText.bind(this);
    this.handlePointerCurve = this.handlePointerCurve.bind(this);
    this.editPointer = this.editPointer.bind(this);
    this.removePointer = this.removePointer.bind(this);
    this.pointerDataExists = this.pointerDataExists.bind(this);
    this.handleStacked = this.handleStacked.bind(this);
    this.handleInterpolation = this.handleInterpolation.bind(this);

    this.handleTips = this.handleTips.bind(this);
    this.state = {
      expanded: this.props.currentAnnotation.type
    };
  }


  helpInterpolation() {
    Swal({
      title: 'Interpolieren?',
      text: 'Interpolation refers to the smoothness and curvature of the line. Try it out!',
      type: 'info',
    });
  }


    handleStacked(event) {
    const stacked = event.target.checked;

    const fields = {
      'options.stacked': stacked
    };

    if (stacked) {
      fields['y_axis.min'] = '';
      fields['y_axis.max'] = '';
    }

    updateAndSave('charts.update.multiple.fields', this.props.chart._id, fields, err => {
      if (err) console.log(err);
    });
  }

  isStacked() {
    return this.isStackableExpandable() && this.props.chart.options.stacked;
  }

  handleInterpolation(event) {
    const interpolation = event.target.value;
    updateAndSave('charts.update.options.interpolation', this.props.chart._id, interpolation);
  }

  isStackableExpandable() {
    const type = this.props.chart.options.type,
      stackableExpandableTypes = ['area', 'bar', 'column'];
    return stackableExpandableTypes.indexOf(type) === -1 ? false : true;
  }

  isLineChartType() {
    const type = this.props.chart.options.type,
      lineTypes = ['area', 'line', 'stream', 'multiline'];
    return lineTypes.indexOf(type) === -1 ? false : true;
  }



  handleTips(event) {
    const tips = event.target.checked;
    updateAndSave('charts.update.options.tips', this.props.chart._id, tips);
  }

  expandStatus(category) {
    return this.state.expanded === category ? 'expanded' : 'collapsed';
  }

  resetDefaultAnnoSettings(key, val) {
    const keyArr = [key],
      valArr = [val];
    Object.keys(this.props.defaultAnnoSettings).map(key => {
      keyArr.push(key);
      let val;
      if (key === 'rangeAxis') {
        const scaleTypeX = this.props.chart.x_axis.scale,
          scaleTypeY = this.props.chart.y_axis.scale;
        if (scaleTypeX === 'ordinal' || scaleTypeY === 'ordinal') {
          val = scaleTypeX === 'ordinal' ? 'y' : 'x';
        } else {
          val = this.props.defaultAnnoSettings[key];
        }
      } else {
        val = this.props.defaultAnnoSettings[key];
      }
      valArr.push(val);
    });
    return [keyArr, valArr];
  }

  toggleSubsectionExpand(event) {
    const type = event.currentTarget.id,
      [keyArr, valArr] = this.resetDefaultAnnoSettings('type', type);

    this.props.handleCurrentAnnotation(keyArr, valArr);
    this.setState({ expanded: type });
  }

  displayHighlight() {

    const { data } = dataParse(this.props.chart.data);

    let needsDates;

    if (this.props.chart.options.type !== 'bar') {
      needsDates = this.props.chart.x_axis.scale === 'ordinal' ? undefined : this.props.chart.date_format;
    }

    let dataObj, error;

    try {
      dataObj = parse(data, needsDates, this.props.chart.options.indexed);
    } catch (e) {
      error = e;
    }

    if (error) return;

    if (this.props.chart.options.type === 'bar' || this.props.chart.options.type === 'column') {
      return dataObj.seriesAmount === 1;
    } else {
      return false;
    }
  }

  highlightColors() {
    if (app_settings) return app_settings.highlightOptions;
  }

  currentAnno(type) {
    const chart = this.props.chart;
    if (chart.annotations && chart.annotations[type] && chart.annotations[type].length) {
      return true;
    } else {
      return false;
    }
  }

  removeHighlight(event) {
    const key = event.target.value;
    const h = this.props.chart.annotations.highlight.filter(d => {
      if (d.key !== key) return d;
    });
    updateAndSave('charts.update.annotation.highlight', this.props.chart._id, h);
  }

  setRangeConfig(event) {
    this.props.handleCurrentAnnotation(
      ['currId', event.target.name, 'rangeStart', 'rangeEnd'],
      ['', event.target.value, '', '']
    );
  }

  formatRangeValue(range) {
    const data = this.props.currentAnnotation[range];

    if (!data) return '';

    const axis = this.props.chart[`${this.props.currentAnnotation.rangeAxis}_axis`];

    if (axis.scale === 'time' || axis.scale === 'ordinal-time') {
      return data;
    } else {

      // if it's direct input, let it be whatever the user wants
      if (event.type === 'input') return data;

      const rangeFormatting = {
        auto: 100,
        comma: 100,
        general: 100,
        round1: 10,
        round2: 100,
        round3: 1000,
        round4: 10000
      };

      return Math.round(Number(data) * rangeFormatting[axis.format]) / rangeFormatting[axis.format];
    }
  }

  setRangeValue(event) {

    const rangePosition = event.target.id,
      rangeValue = event.target.value,
      id = this.props.currentAnnotation.currId;

    if (!isNumber(rangeValue)) return;

    this.props.handleCurrentAnnotation(rangePosition, rangeValue);

    if (isNumber(id)) {
      const range = this.props.chart.annotations.range.slice(),
        key = rangePosition === 'rangeStart' ? 'start' : 'end';
      range[id][key] = rangeValue;
      updateAndSave('charts.update.annotation.range', this.props.chart._id, range);
    }
  }

  renderRangeValueInput(type) {

    const rangeAxis = this.props.currentAnnotation.rangeAxis,
      rangeType = this.props.currentAnnotation.rangeType,
      scaleType = this.props.chart[`${rangeAxis}_axis`].scale,
      labelText = type === 'rangeStart' ? 'Start' : 'End (optional)';

    return (
      <div className={`range-row-item ${rangeType === 'line' && type === 'rangeEnd' ? 'muted' : ''}`}>
        <label
          className={`range-value ${scaleType === 'linear' ? 'editable' : ''}`}
        >{labelText}</label>
        <input
          id={type}
          type={scaleType === 'linear' ? 'number' : 'text'}
          tabIndex={scaleType === 'linear' ? '' : '-1'}
          readOnly={scaleType === 'linear' ? false : true}
          className={`range-value ${scaleType === 'linear' ? 'editable' : ''}`}
          value={this.formatRangeValue(type)}
          onChange={this.setRangeValue}
        />
      </div>
    );

  }

  editRange(event) {
    const range = this.props.chart.annotations.range.slice(),
      id = parseInt(event.target.value),
      item = range[id];

    const keyArr = [
      'rangeStart',
      'rangeEnd',
      'rangeAxis',
      'rangeType',
      'currId'
    ];

    const valueArr = [
      item.start,
      item.end,
      item.axis,
      item.end ? 'area' : 'line',
      id
    ];

    this.props.handleCurrentAnnotation(keyArr, valueArr);
  }

  removeRange(event) {
    const range = this.props.chart.annotations.range.slice();
    range.splice(Number(event.target.value), 1);
    updateAndSave('charts.update.annotation.range', this.props.chart._id, range);
    this.props.handleCurrentAnnotation(
      ['currId', 'rangeStart', 'rangeEnd', 'rangeAxis', 'rangeType'],
      ['', '', '', 'x', 'area']
    );
  }

  setTextConfig(event) {
    this.props.handleCurrentAnnotation(event.target.id, event.target.value);

    const id = this.props.currentAnnotation.currId;

    if (isNumber(id)) {
      const text = this.props.chart.annotations.text.slice();
      text[id][event.target.id === 'textAlign' ? 'text-align' : 'valign'] = event.target.value;
      updateAndSave('charts.update.annotation.text', this.props.chart._id, text);
    }
  }

  editText(event) {
    const text = this.props.chart.annotations.text.slice(),
      id = parseInt(event.target.value),
      item = text[id];

    const keyArr = [
      'textText',
      'textAlign',
      'textValign',
      'textX',
      'textY',
      'currId'
    ];

    const valueArr = [
      item.text,
      item['text-align'],
      item.valign,
      Number(item.position.x),
      Number(item.position.y),
      id
    ];

    this.props.handleCurrentAnnotation(keyArr, valueArr);
  }

  removeText(event) {
    const text = this.props.chart.annotations.text.slice();
    text.splice(Number(event.target.value), 1);
    updateAndSave('charts.update.annotation.text', this.props.chart._id, text);
    this.props.handleCurrentAnnotation(
      ['currId', 'textText', 'textX', 'textY', 'textAlign', 'textValign'],
      ['', '', '', '', 'left', 'top']
    );
  }

  handlePointerCurve(value) {
    this.props.handleCurrentAnnotation('pointerCurve', value);

    const id = this.props.currentAnnotation.currId;

    if (isNumber(id)) {
      const pointer = this.props.chart.annotations.pointer.slice();
      pointer[id].curve = value;
      updateAndSave('charts.update.annotation.pointer', this.props.chart._id, pointer);
    }

  }

  editPointer(event) {

    const pointer = this.props.chart.annotations.pointer.slice(),
      id = parseInt(event.target.value),
      item = pointer[id];

    const keyArr = [
      'pointerCurve',
      'pointerX1',
      'pointerY1',
      'pointerX2',
      'pointerY2',
      'currId'
    ];

    const valueArr = [
      item.curve,
      Number(item.position[0].x),
      Number(item.position[0].y),
      Number(item.position[1].x),
      Number(item.position[1].y),
      id
    ];

    this.props.handleCurrentAnnotation(keyArr, valueArr);

  }

  removePointer(event) {
    const pointer = this.props.chart.annotations.pointer.slice();
    pointer.splice(Number(event.target.value), 1);
    updateAndSave('charts.update.annotation.pointer', this.props.chart._id, pointer);
    this.props.handleCurrentAnnotation(
      ['pointerCurve', 'pointerX1', 'pointerY1', 'pointerX2', 'pointerY2', 'currId'],
      [0.3, '', '', '', '', '']
    );
  }

  resetAnnotations() {
    updateAndSave('charts.reset.annotation', this.props.chart._id);
  }

  helpTips() {
    Swal({
      title: 'Mouse Over anzeigen?',
      text: ' Werte im Diagramm beim Darüberfahren anzeigen. ',
      type: 'info',
    });
  }

  helpStacked() {
    Swal({
      title: 'Gestapelt',
      text: 'Auswählen, um Balken oder Säulendiagramme gestapelt und die Werte kumulativ anzuzeigen.',
      type: 'info',
    });
  }


  helpHighlighting(event) {
    event.stopPropagation();
    Swal({
      title: 'Einfärben',
      text: 'Balken können selbständig eingefärbt werden. Erst Farbe auswählen, dann den jeweiligen Balken per Klick füllen',
      type: 'info'
    });
  }

  helpPointer(event) {
    event.stopPropagation();
    Swal({
      title: 'Pfeile',
      text: 'Hier können Pfeile über den Bildschrim gezeichnet werden.',
      type: 'info'
    });
  }

  helpRanges(event) {
    event.stopPropagation();
    Swal({
      title: 'Ranges and lines',
      text: 'You can click and drag on the chart to create a custom range annotation across the x- or y-axis.',
      type: 'info'
    });
  }

  helpText(event) {
    event.stopPropagation();
    Swal({
      title: 'Text',
      text: 'Hiermit kann ein Textfeld auf dem Diagramm eingefügt werden.',
      type: 'info'
    });
  }

  pointerDataExists() {
    const anno = this.props.currentAnnotation;
    return anno.pointerX1 && anno.pointerY1 && anno.pointerX2 && anno.pointerY2;
  }

  render() {
    return (
      <div className='edit-box'>
        <h3 id='ChartAnnotations' onClick={this.props.toggleCollapseExpand}>Beschriftung & Markierung</h3>
        <div className={`unit-edit ${this.props.expandStatus('ChartAnnotations')}`}>

          {this.props.annotationMode ?
            <p className='note'>Während der der Beschriftungs - Tab offen ist, sind die anderen Beschriftungen ausgeschaltet. Hier könnte noch eine genauere Erklärung stehen.. </p> : null
          }

          <div className='unit-edit unit-anno anno-text-edit'>
            <h4 id='text' onClick={this.toggleSubsectionExpand}><span className='anno-subhed'>Text</span> <a onClick={this.helpText} className='help-toggle help-anno-text'>?</a></h4>
            <div className={`unit-annotation-expand ${this.expandStatus('text')}`}>
              <div className='add-text'>
                <div className='text-row'>
                  <div className='text-row-item'>
                    <label htmlFor='textAlign'> Ausrichtung</label>
                    <div className='select-wrapper'>
                      <select
                        id='textAlign'
                        className='select-textalign'
                        name='textAlign'
                        value={this.props.currentAnnotation.textAlign}
                        onChange={this.setTextConfig}
                      >
                        {['Left', 'Middle', 'Right'].map(f => {
                          return <option key={f} value={f.toLowerCase()}>{f}</option>;
                        })}
                      </select>
                    </div>
                  </div>
                  <div className='text-row-item'>
                    <label htmlFor='textValign'>Vertikale Ausrichtung</label>
                    <div className='select-wrapper'>
                      <select
                        id='textValign'
                        className='select-textvalign'
                        name='textValign'
                        value={this.props.currentAnnotation.textValign}
                        onChange={this.setTextConfig}
                      >
                        {['Top', 'Middle', 'Bottom'].map(f => {
                          return <option key={f} value={f.toLowerCase()}>{f}</option>;
                        })}
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              {this.currentAnno('text') ?
                <div className='current-text'>
                  <p>Current text annotations</p>
                  <ul>
                    {this.props.chart.annotations.text.map((d, i) => {
                      let keyId;
                      if (this.props.currentAnnotation.type === 'text') {
                        keyId = this.props.currentAnnotation.currId;
                      }
                      return (
                        <li
                          className={`text-item ${i === keyId ? 'text-item-selected' : ''}`}
                          key={i}
                        >
                          <p>{d.text}</p>
                          <div className='text-tools'>
                            <button className='text-edit' value={i} onClick={this.editText}>Edit</button>
                            <button className='text-remove' value={i} onClick={this.removeText}>&times;</button>
                          </div>
                        </li>
                      );
                    })}
                  </ul>
                </div>
                : null }
            </div>
          </div>

          <div className='unit-edit unit-anno anno-pointer-edit'>
            <h4 id='pointer' onClick={this.toggleSubsectionExpand}><span className='anno-subhed'>Pfeile</span> <a onClick={this.helpPointer} className='help-toggle help-anno-pointer'>?</a></h4>
            <div className={`unit-annotation-expand ${this.expandStatus('pointer')}`}>
              <div className='add-pointer'>
                <div className='pointer-row'>
                  <div className='pointer-row-item'>
                    <label>Pointer curviness</label>
                    <Slider
                      min={-1}
                      max={1}
                      value={this.props.currentAnnotation.pointerCurve}
                      step={0.1}
                      onChange={this.handlePointerCurve}
                    />
                  </div>
                </div>
              </div>

              {this.currentAnno('pointer') ?
                <div className='current-pointer'>
                  <p>Gezeichnete Pfeile</p>
                  <ul>
                    {this.props.chart.annotations.pointer.map((d, i) => {
                      let keyId;
                      if (this.props.currentAnnotation.type === 'pointer') {
                        keyId = this.props.currentAnnotation.currId;
                      }
                      return (
                        <li
                          className={`pointer-item ${i === keyId ? 'pointer-item-selected' : ''}`}
                          key={i}
                        >
                          <p>Pointer {i + 1}</p>
                          <div className='pointer-tools'>
                            <button className='pointer-edit' value={i} onClick={this.editPointer}>Edit</button>
                            <button className='pointer-remove' value={i} onClick={this.removePointer}>&times;</button>
                          </div>
                        </li>
                      );
                    })}
                  </ul>
                </div>
                : null }
            </div>
          </div>

          { this.displayHighlight() ?
            <div className='unit-edit unit-anno anno-highlight-edit'>
              <h4 id='highlight' onClick={this.toggleSubsectionExpand}><span className='anno-subhed'>Einzelne Flächen farbig Markieren</span> <a onClick={this.helpHighlighting} className='help-toggle help-anno-higlight'>?</a></h4>
              <div className={`unit-annotation-expand ${this.expandStatus('highlight')}`}>
                <ColorPicker
                  triangle={'hide'}
                  colors={app_settings.highlightOptions}
                  onChangeComplete={this.props.handleHighlightColor}
                  color={this.props.currentAnnotation.highlight}
                  width={'100%'}
                  className={'color-picker'}
                />
                {this.currentAnno('highlight') ?
                  <div className='current-highlight'>
                    <p> Eingefärbte Flächen </p>
                    <ul>
                      {this.props.chart.annotations.highlight.map(d => {
                        return (
                          <li className='highlight-item' key={d.key}>
                            <div className='highlight-color' style={{ backgroundColor: d.color }}>
                              <button className='highlight-remove' value={d.key} onClick={this.removeHighlight}>&times;</button>
                            </div>
                            <div className='highlight-key'>{d.key}</div>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                  : null }
              </div>
            </div>
            : null }


          <div className='unit-edit tips-edit'>
            <h4>Mouse Over anzeigen<a onClick={this.helpTips} className='help-toggle help-tips'>?</a></h4>
            <input
              className='input-checkbox-tips'
              type='checkbox'
              name='Tips'
              checked={this.props.chart.options.tips}
              onChange={this.handleTips}
            />
          </div>

             { this.isStackableExpandable() ?
            <div className='unit-edit stacked-edit'>
              <h4>Gestapelt <a onClick={this.helpStacked} className='help-toggle help-stacked'>?</a></h4>
              <input
                className='input-checkbox-stacked'
                type='checkbox'
                name='isStacked'
                onChange={this.handleStacked}
                checked={this.isStacked()}
              />
            </div>
            : null }
          { this.isLineChartType() ?
            <div className='unit-edit interpolation-edit'>
              <h4>Interpolieren <a onClick={this.helpInterpolation} className='help-toggle help-interpolation'>?</a></h4>
              <div className='select-wrapper'>
                <select
                  className='select-interpolation'
                  value={this.props.chart.options.interpolation}
                  onChange={this.handleInterpolation}
                >
                  {interpolations.map(d => {
                    return <option key={d.format} value={d.format}>{d.pretty}</option>;
                  })}
                </select>
              </div>
            </div>
            : null }

          <button className='annotation-reset' onClick={this.resetAnnotations}>Alle Beschriftungen und Markierungen löschen</button>

        </div>
      </div>
    );
  }

}
