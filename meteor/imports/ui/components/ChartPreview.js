import React, { Component } from 'react';
import Chart from './Chart';
import { renderLoading } from '../../modules/utils';
import slugify from 'slug';
import { Link } from 'react-router-dom';
import { app_settings } from '../../modules/settings';
import { setDocumentTitle } from '../../modules/utils';
import { DebounceInput } from 'react-debounce-input';

export default class ChartPreview extends Component {

  constructor(props) {
    super(props);
    this.setSlugValue = this.setSlugValue.bind(this);
    this.updateSlug = this.updateSlug.bind(this);
  }

  componentDidMount() {
    const slug = this.props.chart && this.props.chart.slug ? this.props.chart.slug : undefined;
    document.title = setDocumentTitle(this.props.match.path, slug);
  }

  componentDidUpdate() {
    const slug = this.props.chart && this.props.chart.slug ? this.props.chart.slug : undefined;
    document.title = setDocumentTitle(this.props.match.path, slug);
  }

  setSlugValue(slug) {
    Meteor.call('charts.update.slug', this.props.match.params._id, slug, err => {
      if (err) console.log(err);
    });
  }

  updateSlug(event) {
    const slugData = event.target.value,
      slug = slugify(slugData);
    event.target.value = slug;
    if (slug) this.setSlugValue(slug);
  }

  renderEditSlug() {
    return (
        <div>
          Dateiname:
          <DebounceInput
            minLength={2}
            debounceTimeout={500}
            element='input'
            className='input-slug-edit'
            placeholder={this.props.chart.slug}
            type='text'
            name='slug'
            onChange={this.updateSlug}
            forceNotifyByEnter={false}
            value={this.props.chart.slug}
          />
        </div>
    );
  }




  render() {
    const previewClass = this.props.annotationMode ?
      `chart-preview chart-preview-annotation chart-preview-annotation-${this.props.currentAnnotation.type}` :
      'chart-preview';
    return (
      <div className={previewClass}>
        <div className='desktop-preview'>
          <h5> Vorschau <span>{this.props.annotationMode ? `Im Modus Beschriftung und Markierung: ${this.props.currentAnnotation.type}` : ''}
            <div className='chart-slug'>
          {this.renderEditSlug()}
          </div>
          </span>
        


          </h5>
          { !this.props.loading ?
            <Chart
              type={'desktop'}
              chart={this.props.chart}
              annotationMode={this.props.annotationMode}
              currentAnnotation={this.props.currentAnnotation}
              editable={true}
              exportable={false}
              share_data={false}
              social={false}
              {...this.props}
            /> : renderLoading()
          }
        </div>
      </div>
    );
  }

}
