import React, { Component } from 'react';
import { updateAndSave } from '../../modules/utils';
import { app_settings } from '../../modules/settings';
import Swal from 'sweetalert2';

const interpolations = [
  { format: 'linear', pretty: 'Linear' },
  { format: 'step', pretty: 'Stufen' },
  { format: 'step-before', pretty: 'Stufe davor' },
  { format: 'step-after', pretty: 'Stufe danach' },
  { format: 'natural', pretty: 'Natürlich' }
];

export default class ChartStyling extends Component {

  constructor(props) {
    super(props);
    this.handlePalette = this.handlePalette.bind(this);
    // this.handleStacked = this.handleStacked.bind(this);
    // this.handleInterpolation = this.handleInterpolation.bind(this);
  }

  handlePalette(event) {
    const palette = event.target.value;
    updateAndSave('charts.update.class', this.props.chart._id, palette);
  }

  // handleStacked(event) {
  //   const stacked = event.target.checked;

  //   const fields = {
  //     'options.stacked': stacked
  //   };

  //   if (stacked) {
  //     fields['y_axis.min'] = '';
  //     fields['y_axis.max'] = '';
  //   }

  //   updateAndSave('charts.update.multiple.fields', this.props.chart._id, fields, err => {
  //     if (err) console.log(err);
  //   });
  // }

  // isStacked() {
  //   return this.isStackableExpandable() && this.props.chart.options.stacked;
  // }

  // handleInterpolation(event) {
  //   const interpolation = event.target.value;
  //   updateAndSave('charts.update.options.interpolation', this.props.chart._id, interpolation);
  // }

  // isStackableExpandable() {
  //   const type = this.props.chart.options.type,
  //     stackableExpandableTypes = ['area', 'bar', 'column'];
  //   return stackableExpandableTypes.indexOf(type) === -1 ? false : true;
  // }

  // isLineChartType() {
  //   const type = this.props.chart.options.type,
  //     lineTypes = ['area', 'line', 'stream', 'multiline'];
  //   return lineTypes.indexOf(type) === -1 ? false : true;
  // }

  // helpStacked() {
  //   Swal({
  //     title: 'Gestapelt?',
  //     text: 'Check this box to toggle series stacking and visualize cumulative data.',
  //     type: 'info',
  //   });
  // }

  // helpInterpolation() {
  //   Swal({
  //     title: 'Interpolieren?',
  //     text: 'Interpolation refers to the smoothness and curvature of the line. Try it out!',
  //     type: 'info',
  //   });
  // }

  render() {
    return (
      <div className='edit-box'>
        <h3 id='ChartStyling' onClick={this.props.toggleCollapseExpand}>Farbstil</h3>
        <div className={`unit-edit ${this.props.expandStatus('ChartStyling')}`}>
          <div className='unit-edit color-edit'>
            <h4>Wähle eine der Farbpaletten</h4>
            <div className='radio-buttons'>
              <ul>
                {app_settings.palettes.map(d => {
                  const p = d.toLowerCase();
                  return (
                    <li key={p}>
                      <input
                        id={`color-${p}`}
                        type='radio'
                        name='color'
                        value={p}
                        className={`input-radio input-radio-class input-radio-${p}`}
                        checked={this.props.chart.class === p}
                        value={p}
                        onChange={this.handlePalette}
                      />
                      <label htmlFor={`color-${p}`}>{p}</label>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
       
        </div>
      </div>
    );
  }

}
