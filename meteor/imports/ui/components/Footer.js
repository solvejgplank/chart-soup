import React, { Component } from 'react';
import { app_version, app_build } from '../../modules/settings';

export default class Footer extends Component {
  render() {
    return (
      <footer>
        <p className='version'><a href="http://www.dw.com">DW Infografik {`${app_version} build ${app_build}`}</a></p>
      </footer>
    );
  }
}
