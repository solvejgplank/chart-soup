import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { updateAndSave } from '../../modules/utils';
import Swal from 'sweetalert2';

export default class ChartOptions extends Component {

  constructor(props) {
    super(props);
    // this.handleShareData = this.handleShareData.bind(this);
    // this.handleSocial = this.handleSocial.bind(this);
    // this.handleTips = this.handleTips.bind(this); --> bei ChartAnnotations.js jetzt 
    this.handleDelete = this.handleDelete.bind(this);
  }

  // handleShareData(event) {
  //   const shareData = event.target.checked;
  //   updateAndSave('charts.update.options.share_data', this.props.chart._id, shareData);
  // }

  // handleSocial(event) {
  //   const social = event.target.checked;
  //   updateAndSave('charts.update.options.social', this.props.chart._id, social);
  // }

  // handleTips(event) {
  //   const tips = event.target.checked;
  //   updateAndSave('charts.update.options.tips', this.props.chart._id, tips);
  // }

  handleDelete() {
    Swal({
      title: 'Möchtest du diese Infografik löschen?',
      text: "Gelöschte Infografiken können nicht wiederhergestellt werden.",
      type: 'warning',
      confirmButtonText: 'Infografik löschen',
      cancelButtonText: 'Abbrechen',
      showCancelButton: true
    }).then(result => {
      if (result.value) {
        Swal({
          title: 'Gelöscht!',
          text: 'Die Infografik wurde gelöscht',
          type: 'success'
        });
        Meteor.call('charts.delete', this.props.chart._id, err => {
          if (err) {
            console.log(err);
          } else {
            this.props.history.push({ pathname: `/archive` });
          }
        });
      }
    });
  }

  // helpShareData() {
  //   Swal({
  //     title: 'Daten verfügbar als Download?',
  //     text: "Adds a 'data' button to each chart which can be toggled to present the charts data in a tabular form along with buttons allowing the raw data to be downloaded.",
  //     type: 'info',
  //   });
  // }

  // helpSocial() {
  //   Swal({
  //     title: 'Auf Social Media teilen?',
  //     text: "Adds a 'social' button to each chart which can be toggled to present the user with social sharing options.",
  //     type: 'info',
  //   });
  // }

  // helpTips() {
  //   Swal({
  //     title: 'Mouse Over anzeigen?',
  //     text: ' Werte im Diagramm beim Darüberfahren anzeigen. ',
  //     type: 'info',
  //   });
  // }

  render() {
    return (
      <div className='edit-box'>
        <h3 id='ChartOptions' onClick={this.props.toggleCollapseExpand}>Löschen</h3>
        <div className={`unit-edit ${this.props.expandStatus('ChartOptions')}`}>

          <button onClick={this.handleDelete} className='unit-edit unit-delete'>Infografik löschen</button>
        </div>
      </div>
    );
  }

}
