import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import Chart from '../components/Chart';
import Charts from '../../api/Charts/Charts';
import { withTracker } from 'meteor/react-meteor-data';
import { timeSince, prettyCreatedAt, renderLoading } from '../../modules/utils';

class ArchiveCharts extends Component {

  constructor(props) {
    super(props);
    this.forkChart = this.forkChart.bind(this);
    this.state = {
      matchedCharts: 0
    };
  }


  forkChart() {
    Meteor.call('charts.fork', this.props.id, (err, result) => {
      if (err) {
        console.log(err);
      } else {
        this.props.history.push({
          pathname: `/chart/${result}/edit`,
          state: { id: result }
        });
      }
    });
  }


  componentWillReceiveProps(nextProps) {
    if (this.props.params !== nextProps.params) {
      Meteor.call('charts.matched.count', this.props.params, (err, res) => {
        if (!err) this.setState({ matchedCharts: res });
      });
    }
  }

  goToChart(id) {
    this.props.history.push({ pathname: `/chart/${id}` });
  }

  render() {
    return (
      <section className='charts-archive_results'>
        <div className='charts-archive_count'>
          <div className='charts-archive_count-left'>
            <h3> <span>{ this.props.charts.length }</span> von <span>{ this.state.matchedCharts }</span> Infografiken</h3>
          </div>
          <div className='charts-archive_count-right'>
          <h3>Maximale Anzahl pro Seite: </h3>
            <select
              className='charts-archive_count-limit'
              value={this.props.limit}
              onChange={this.props.setLimit}>
              {[24, 48, 96].map(l => <option key={l} value={l}>{l}</option>)}
            </select>
          </div>
        </div>
        <div className='charts-archive_grid'>
          {this.props.charts.map(chart => {
            return (
              <div
                className='charts-archive_single'
                key={chart._id}
                onClick={() => this.goToChart(chart._id)}
                >
                <div className='charts-archive_single-inner'>
                  <h4 className='slug'>{chart.slug}</h4>
                  <h6 id='spc' className='chart-show_fork' onClick={this.forkChart}>Bearbeiten</h6>
                  {chart.img ?
                    <img src={chart.img} /> :
                    <div className='empty-image'><p>Keine Bild der Infografik vorhanden</p></div>
                  }
                  
                  
                </div>
              </div>
            );
          })}
          <div className='charts-archive_empty-container'>
            <div className='charts-archive_empty'>
              <img src='/images/error.svg' className='chart-archive_empty-img'/>
              <h2>Keine Infografik gefunden.</h2>
              <p>Wir konnten keine Infografik mit diesen Kriterien finden. </p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withTracker({
  getMeteorData: props => {
    const params = {
      queryName: 'chart.archive',
      filters: {
        search: props.search,
        types: props.types,
        tags: props.tags,
      },
      limit: props.limit
    };
    const subscription = Meteor.subscribe('chart.archive', params),
      options = { sort: {} };
    options.sort = [[props.sort.field, props.sort.order]];
    return {
      params: params,
      loading: !subscription.ready(),
      charts: Charts.find({}, options).fetch(),
      setLimit: props.setLimit,
      search: props.search,
      types: props.types,
      tags: props.tags,
      limit: props.limit,
      history: props.history
    };
  },
  pure: false
})(ArchiveCharts);
