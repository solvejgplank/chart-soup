import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
//ab hier kopiert
import slugify from 'slug';
import { Link } from 'react-router-dom';
import { app_settings } from '../../modules/settings';
import { setDocumentTitle } from '../../modules/utils';
import { DebounceInput } from 'react-debounce-input';
// bis hier
export default class Header extends Component {

  constructor(props) {
    super(props);
    this.setSlugValue = this.setSlugValue.bind(this);
    this.updateSlug = this.updateSlug.bind(this);
  }

  componentDidMount() {
    const slug = this.props.chart && this.props.chart.slug ? this.props.chart.slug : undefined;
    document.title = setDocumentTitle(this.props.match.path, slug);
  }

  componentDidUpdate() {
    const slug = this.props.chart && this.props.chart.slug ? this.props.chart.slug : undefined;
    document.title = setDocumentTitle(this.props.match.path, slug);
  }

  setSlugValue(slug) {
    Meteor.call('charts.update.slug', this.props.match.params._id, slug, err => {
      if (err) console.log(err);
    });
  }

  updateSlug(event) {
    const slugData = event.target.value,
      slug = slugify(slugData);
    event.target.value = slug;
    if (slug) this.setSlugValue(slug);
  }

  renderEditSlug() {
    return (
        <div className='chart-slug'>
           { this.renderNav() }
        <label>
          <DebounceInput
            minLength={2}
            debounceTimeout={500}
            element='input'
            className='old-input-slug-edit'
            placeholder={this.props.chart.slug}
            type='text'
            name='slug'
            onChange={this.updateSlug}
            forceNotifyByEnter={false}
            value={this.props.chart.slug}
          />
        </label>
        </div>
    );
  }

  renderNav() {
    return (
      <div className={this.props.edit ? 'header-edit-nav' : 'header-nav'}> 
        <div className='header-list'><Link to='/archive'>
        <img src={'/images/Archiv.svg'} className="img-Archiv"/>
          Archiv</Link></div>
        <div className='header-new'><Link to='/new'>
        <img src={'/images/Kreuz.svg'} className="img-Kreuz"/>
          Neue Infografik</Link></div>
      </div>
    );
  }

  render() {
    return (
      <header >
        <div className={this.props.edit ? 'header-edit' : ''}>
          <img src={'/images/fliege1.svg'} className="fliege"/>
        </div>        

        <div>
          {this.props.edit ? this.renderEditSlug() : this.renderNav()}
        </div>
      </header>
    );
  }
}