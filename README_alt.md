# Chart SOUP - das DW Infografik Tool 

Die Chart SOUP der DW erzeugt Infografiken aus eigenen Daten und lässt diese vom Nutzer übersichtlich in einer Datenbank verwalten. Inhalte können durch verschiedenen Charttypen (Balken, Linien, Flächen, Donut) visualisiert werden. Die fertige Infografik kann als responsive SVG-Datei exportiert werden. Auf der Webseite der Deutschen Welle werden diese aus dem CMS eingebettet. Um den Anforderungen und dem CI des Hauses zu entsprechen, orientiert sich das Design der Grafiken den strikten Vorgaben des Hauses. 

Die Chart SOUP kann Diagramme in unterschiedlichen Formaten exportieren:

* **SVGs** die sowohl responsiv als auch interaktiv sind, können als Embed Code in Artikel eingebaut werden
* **JPGs und PNGs** für den Einsatz auf Social Media 
* **PDFs** mit anwählbaren Ebenen, für die Weiterverarbeitung zu Animationen in After Effects oder anderen Tools

----------
## Inhaltsverzeichnis

* [Aufbau und Einbindung externer Tools](#aufbau-und-Einbindung-externer-Tools)
* [Funktionen](#funktionen)
* [Beispiele](#beispiele)
* [Loslegen](#loslegen)
* [Tutorials](#tutorials)
* [Version](#version)
* [Lizenz](#Lizenz)
* [Über Uns](#über-uns)

----------


## Aufbau und Einbindung externer Tools

Aufgeteilt in ein Front End und ein Back End: aus welchen Komponenten ist der Editor zusammengebaut? 

**Die Back End Schnittstelle** um die Daten speichern und verwalten zu können und um daraus Diagramme zu erzeugen und exportieren oder auch einen statischen Embed-Code zu generieren, verwenden wir den Service von  [Meteor](https://www.meteor.com/) und [React](https://reactjs.org/). 
Außerdem ist das Widget noch gesichert: falls das Backend mal ausfällt, gibt es eine statische Sicherung als JPG, die in jedem Fall richtig angezeigt wird. 

**Im Front-end eine JavaScript und eine CSS Bibliothek** das den Embed Code aus dem Back End analysiert und daraus eine responsive Grafik erstellt, die auf der Website ausgespielt werden kann. Dabei verwenden wir Teile von [D3.js](http://d3js.org). Das Design der Benutzeroberfläche wurde mit CSS gestyled, basierend auf React Komponenten. 


## Funktionen

* **Eigenes Verzeichnis:** Alle bereits erzeugten Diagramme können mit beliebig vielen Schlagwörtern / Tags gekennzeichnet werden, in einer Datenbank gespeichert und dadurch später leichter gesucht werden.
* **Einfache, selbsterklärende GUI:** Easy-to-use (and easy-to-teach!) collaborative interface for editing charts, so multiple people can work on the same chart at once
* **Asset generation:** Easily generate PNGs at any size for use on social media, or PDFs for print use
* **Fallback images:** All charts come with a fallback image in case the library is unable to draw the chart. Fallbacks can be stored on AWS, or bundled as inline Base64 images
* **Fully responsive:** Charts will redraw automatically on window resize
* **Charttypen:**
  * Liniendiagramm
  * Flächediagramm
  * Gestapeltes Flächendiagramm
  * Säulendiagramm
  * Gestapeltes Säulendiagramm
  * Balkendiagramm
  * gesatpeltes Balkendiagramm
  * Mehrfaches Liniendiagramm
  * Donutgrafik (noch nicht verfügbar, sorry!)
      
          * **Beschriftungen und Markierungen:** Bei dem Typ Balken- oder Säulendiagramm können einzelne Elemente extra eingefärbt werden. Text kann immer eingefügt werden, oder Pfeile, oder die Option des Mouse-Overs kann an- oder ausgewählt werden.
              * **Konfigurierbar:** 
                  * **"Abhängig von Zeit" :** Werden Datumsangaben verwendet kann mit einem Häkchen die Zeit in Jahren oder Monaten angegeben werden. 
                      * **Pre- and post-render hooks:** All charts come with several basic custom events via D3's [dispatch library](https://github.com/d3/d3-dispatch), including pre- and post-render hooks for every chart
                          * **Keine Abhängigkeiten zu externen Dienstleistern:** In die SOUP Bibliothek ist bereit alles integriert und installiert. Alles was du dafür brauchst, kommt gebündelt aus einer Box. 
                              * **Leichte Programmierung:** Klein und gepackt.. 
                                  
                                      
                                          ## Beispiele
                                              
                                                  
                                                      ## Vorbereitung und Installation 
                                                          
                                                              * Wenn du das Programm und mit allen Packages auf deinem Rechner installieren möchtest, benötigst du Adminrechte an deinem DW Arbeitsplatz. Öffne dafür das Terminal. 
                                                                  
                                                                      Installiere vorab den den Node Managaer zur Versionskontrolle [nvm](https://github.com/creationix/nvm). Es ist ganz wichtig, immer die Version zu verwenden, die Chart-Soup erwartet, damit das Programm läuft.
                                                                      
                                                                      Installiere [Node.js](https://nodejs.org) In der Version 8.11, keine andere Version, sonst läuft das Tool nicht.
                                                                          
                                                                              Installiere [Meteor, wie auf der Webseite beschrieben](https://www.meteor.com/install).
                                                                                  
                                                                                      Installiere [Gulp] (http://gulpjs.com/).
                                                                                          
                                                                                              ```sh
                                                                                                  $ npm install -g gulp
                                                                                                     ```
                                                                                                         
                                                                                                             Klone jetzt unser Chart-Soup Repository von Bitbucket und installiere die NPM dependencies für den Root Ordner und auch und fürs Backend, um sicherzustellen, das Gulp richtig konfiguriert ist, und das Programm damit läuft:
                                                                                                                 
                                                                                                                     ```sh
                                                                                                                         $ git clone [git-repo-url] && cd chart-soup
                                                                                                                             $ npm install
                                                                                                                                 ```
                                                                                                                                    
                                                                                                                                         Danach, welchselst du in den Meteor Ordner :
                                                                                                                                           
                                                                                                                                                 ```sh
                                                                                                                                                   $ cd meteor 
                                                                                                                                                      ```
                                                                                                                                                        
                                                                                                                                                          und startest erstmals 
                                                                                                                                                            
                                                                                                                                                              `meteor`,  damit verbindest du das Programm mit der Mongo DB und intsalliert die dependencies. Dies könnte einige Minuten dauern, danach läuft meteor immer von alleine. Stoppe den Meteoserver mit (`CTRL-C` auf dem Mac) und gehe mit  `cd` zurück ins Hauptverzeichnis. Alles was jetzt noch fehlt, ist ein kleines Wort mit vier Buchstaben, dass jedes Mal erneut eingegeben werde muss: gulp. 
                                                                                                                                                                  
                                                                                                                                                                      ```sh
                                                                                                                                                                          $ gulp
                                                                                                                                                                              ```
                                                                                                                                                                                  
                                                                                                                                                                                      Das war's! Jetzt kannst du die Web- Applikation in deinem Server aufrufen, unter
                                                                                                                                                                                          
                                                                                                                                                                                              [![localhost:3000]](http://localhost:3000/ ) 
                                                                                                                                                                                                  Die Chart-Soup kann jetzt in der aktuellen Version, bei dir getestet werden. Glückwunsch! 
                                                                                                                                                                                                      
                                                                                                                                                                                                          
                                                                                                                                                                                                              ## Tutorials
                                                                                                                                                                                                                  
                                                                                                                                                                                                                      
                                                                                                                                                                                                                          ## Version
                                                                                                                                                                                                                              
                                                                                                                                                                                                                                  0.3.1 (15.03.2019)
                                                                                                                                                                                                                                      
                                                                                                                                                                                                                                          
                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                                  ## Lizenz
                                                                                                                                                                                                                                                      
                                                                                                                                                                                                                                                          ???
                                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                                                  
                                                                                                                                                                                                                                                                      ## Über Uns
                                                                                                                                                                                                                                                                          
                                                                                                                                                                                                                                                                              Kontakt
                                                                                                                                                                                                                                                                                  
                                                                                                                                                                                                                                                                                      [![Solvejg Plank](AVATAR)](https://bitbucket.com/eikeik) | 
                                                                                                                                                                                                                                                                                          [![Olof Pock](AVATAR)](https://bitbucket.com/pockpock) | 
                                                                                                                                                                                                                                                                                              [![Olga Urusova](AVATAR)](https://bitbucket.com/olgaurusova) |
                                                                                                                                                                                                                                                                                                  ---|---|---|---
